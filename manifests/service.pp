#Autor: Ruan Oliveira
#Data: 03/10/2016 22h50
#Email: contato@sonictecnologia.com

class moduloApacheSonic::service {

#instala pacotes
package {['httpd']:
  ensure	=> installed,
}

package {['httpd-devel']:
  ensure        => present,
}

#mantem servico rodando
  service { 'httpd.service':
    ensure	=> 'running',
    require	=> Package['httpd'],
  }


#Criação do arquivo httpd.conf
  file {"/etc/httpd/conf/httpd.conf":
  path      => '/etc/httpd/conf/httpd.conf',
    #source  => '/etc/puppetlabs/code/environments/production/manifests/moduloApacheSonic/httpd.conf',
    source  => 'puppet:///modules/apache/httpd.conf',
  ensure    => 'file',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['httpd'],
    #notify  => Service['httpd.service'],
  }

#Criacao do diretorio do site
  file { "/var/www/html/sonic":
  ensure => 'directory',
     owner  => 'apache',
     group  => 'apache',
     mode   => '0755',
  }

#Criacao do index.html do site
  file{"/var/www/html/sonic/index.html":
    owner    => apache,
    group    => apache,
    mode     => '0644',
    loglevel => debug, # reduce noise in Puppet reports
    content  => inline_template("<!DOCTYPE HTML><html lang=\"pt-BR\"><head>    <meta charset=\"UTF-8\">    <title>SONIC TECNOLOGIA</title></head><body>     <h1>Oi mundo cruel!</h1></body></html>"),
    notify   => Service['httpd.service'],
  }

}
