#Autor: Ruan Oliveira
#Data: 03/10/2016 22h50
#Email: contato@sonictecnologia.com

class moduloApacheSonic::factVerApache {

file{"/opt/puppetlabs/puppet/cache/lib/facter/versao_apache.rb":
  ensure    => 'present',
    owner    => root,
    group    => root,
    mode     => '0644',
    loglevel => debug, 
    content  => inline_template("Facter.add(:versao_apache) do setcode \'apachectl -v 2>&1 | cut -d/ -f 2 | head -n1 | cut -d\" \" -f1\' end"),
  }
}
